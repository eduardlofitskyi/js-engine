package com.lofitskyi.jsengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class JsEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsEngineApplication.class, args);
    }

}

