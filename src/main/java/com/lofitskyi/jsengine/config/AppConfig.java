/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@Configuration
public class AppConfig {

    @Bean
    public ScriptEngine getScriptEngine() {
        ScriptEngineManager factory = new ScriptEngineManager();
        return factory.getEngineByName("JavaScript");    }
}
