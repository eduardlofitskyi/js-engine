/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.controller;

import com.lofitskyi.jsengine.entity.RequestStatus;
import com.lofitskyi.jsengine.entity.ScriptInput;
import com.lofitskyi.jsengine.entity.ScriptOutput;
import com.lofitskyi.jsengine.entity.ScriptStatus;
import com.lofitskyi.jsengine.service.RequestService;
import com.lofitskyi.jsengine.service.ScriptService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.script.ScriptException;
import javax.validation.Valid;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/v1/script")
@RequiredArgsConstructor
public class AsyncScriptController {

    @Value("${script.timeout: 60}")
    private Long timeout;

    private final ScriptService scriptService;
    private final RequestService requestService;

    @PutMapping(value = "/run")
    public ResponseEntity<String> runScript(@Valid @RequestBody ScriptInput script, Errors errors) throws ScriptException {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().build();
        }

        RequestStatus request = this.requestService.createRequest(script);

        CompletableFuture<ScriptOutput> scriptOutputCompletableFuture = this.scriptService.runScriptAsync(script);

        request.setStatus(ScriptStatus.IN_PROGRESS);

        requestService.updateRequest(request);

        scriptOutputCompletableFuture
                .orTimeout(timeout, TimeUnit.SECONDS)
                .handle((scriptOut, throwable) -> handleRequestResult(request, scriptOut, throwable));

        return ResponseEntity.accepted().body("submissionId: " + request.getSubmissionId());
    }


    @GetMapping(value = "/{submissionId}")
    public ResponseEntity<RequestStatus> getScriptStatus(@PathVariable String submissionId) {

        Optional<RequestStatus> status = requestService.getRequestStatus(submissionId);

        if (status.isPresent()) {
            return ResponseEntity.ok(status.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{submissionId}")
    public ResponseEntity<RequestStatus> killScript(@PathVariable String submissionId) {
        Optional<RequestStatus> statusOptional = requestService.getRequestStatus(submissionId);

        if (statusOptional.isPresent()) {
            RequestStatus status = statusOptional.get();
            status.setStatus(ScriptStatus.KILLED);
            status.setOutput(StringUtils.EMPTY);
            requestService.updateRequest(status);
            return ResponseEntity.ok(status);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private RequestStatus handleRequestResult(RequestStatus request, ScriptOutput scriptOut, Throwable throwable) {
        if (throwable != null) {
            request.setOutput("Exception: " + throwable.toString());
            request.setStatus(ScriptStatus.FAILED);
        } else {

            if (requestService.isKilled(request)) {
                return requestService.getRequestStatus(request.getSubmissionId()).orElseThrow();
            }
            request.setOutput(scriptOut.getOutput());
            request.setStatus(ScriptStatus.SUCCESS);

        }

        return requestService.updateRequest(request);
    }
}