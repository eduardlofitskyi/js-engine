/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "request_statuses")
@NoArgsConstructor
@Getter @Setter
public class RequestStatus {

    @Id
    private String submissionId;

    private String body;
    private String output;
    private ScriptStatus status;
}
