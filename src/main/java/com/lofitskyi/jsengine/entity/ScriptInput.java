/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ScriptInput {

    @NotNull
    private String body;
    private Long timeoutSec;
}
