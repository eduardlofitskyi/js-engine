/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.entity;

import lombok.Data;

@Data
public class ScriptOutput {

    private String status;
    private String output;
}
