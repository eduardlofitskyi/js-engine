/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.entity;

public enum ScriptStatus {
    QUEUED, IN_PROGRESS, SUCCESS, FAILED, KILLED
}
