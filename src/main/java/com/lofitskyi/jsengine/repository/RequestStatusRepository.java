/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.repository;

import com.lofitskyi.jsengine.entity.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusRepository extends JpaRepository<RequestStatus, String> {
}
