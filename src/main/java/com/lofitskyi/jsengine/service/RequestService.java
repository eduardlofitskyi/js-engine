/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.service;

import com.lofitskyi.jsengine.entity.RequestStatus;
import com.lofitskyi.jsengine.entity.ScriptInput;

import java.util.Optional;

public interface RequestService {
    RequestStatus createRequest(ScriptInput scriptInput);
    Optional<RequestStatus> getRequestStatus(String submissionId);
    RequestStatus updateRequest(RequestStatus request);
    boolean isKilled(RequestStatus request);
}
