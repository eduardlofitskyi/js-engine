/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.service;

import com.lofitskyi.jsengine.entity.RequestStatus;
import com.lofitskyi.jsengine.entity.ScriptInput;
import com.lofitskyi.jsengine.entity.ScriptStatus;
import com.lofitskyi.jsengine.repository.RequestStatusRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService{

    private final RequestStatusRepository repository;

    @Override
    public RequestStatus createRequest(ScriptInput scriptInput){
        String body = scriptInput.getBody();
        String uuid = UUID.randomUUID().toString();

        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setSubmissionId(uuid);
        requestStatus.setBody(body);
        requestStatus.setOutput(StringUtils.EMPTY);
        requestStatus.setStatus(ScriptStatus.QUEUED);

        return repository.save(requestStatus);
    }

    @Override
    public Optional<RequestStatus> getRequestStatus(String submissionId) {
        return repository.findById(submissionId);
    }

    @Override
    public RequestStatus updateRequest(RequestStatus request) {
        return repository.save(request);
    }

    @Override
    public boolean isKilled(RequestStatus request) {
        RequestStatus status = repository.findById(request.getSubmissionId()).orElseThrow();
        return status.getStatus() == ScriptStatus.KILLED;
    }
}
