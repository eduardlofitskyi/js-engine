/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.service;

import com.lofitskyi.jsengine.entity.ScriptInput;
import com.lofitskyi.jsengine.entity.ScriptOutput;

import java.util.concurrent.CompletableFuture;

public interface ScriptService {

    CompletableFuture<ScriptOutput> runScriptAsync(ScriptInput script);
}
