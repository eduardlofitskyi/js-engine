/* (c) copyright IBM 2018 */
package com.lofitskyi.jsengine.service;

import com.lofitskyi.jsengine.entity.ScriptInput;
import com.lofitskyi.jsengine.entity.ScriptOutput;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.StringWriter;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class ScriptServiceImpl implements ScriptService{

    private final ScriptEngine engine;

    @Override
    @Async
    public CompletableFuture<ScriptOutput> runScriptAsync(ScriptInput script) {
        StringWriter out = new StringWriter();
        StringWriter err = new StringWriter();

        engine.getContext().setWriter(out);
        engine.getContext().setErrorWriter(err);

        try {
            engine.eval(script.getBody());
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }

        ScriptOutput scriptOutput = new ScriptOutput();
        scriptOutput.setOutput(out.toString());


        return CompletableFuture.completedFuture(scriptOutput);
    }
}
