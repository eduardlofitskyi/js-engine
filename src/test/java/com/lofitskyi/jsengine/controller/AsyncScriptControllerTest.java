package com.lofitskyi.jsengine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lofitskyi.jsengine.entity.RequestStatus;
import com.lofitskyi.jsengine.entity.ScriptInput;
import com.lofitskyi.jsengine.entity.ScriptStatus;
import com.lofitskyi.jsengine.service.RequestService;
import com.lofitskyi.jsengine.service.ScriptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AsyncScriptController.class)
public class AsyncScriptControllerTest {

    private static final String API_PREFIX = "/api/v1/script";
    private static final String EXISTING_SUBMISSION = "submission-0001";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private ScriptService scriptService;

    @Test
    public void givenNonExistingSubmission_whenGetStatus_then404WebStatus()
            throws Exception {
        mvc.perform(get(API_PREFIX + "/non-existing-submission")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenSubmissionId_whenGetStatus_thenReturnCorrectStatus()
            throws Exception {

        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setSubmissionId(EXISTING_SUBMISSION);
        requestStatus.setBody("print('js')");
        requestStatus.setStatus(ScriptStatus.IN_PROGRESS);

        given(requestService.getRequestStatus(EXISTING_SUBMISSION)).willReturn(Optional.of(requestStatus));

        mvc.perform(get(API_PREFIX + "/" + EXISTING_SUBMISSION)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.submissionId", is(EXISTING_SUBMISSION)))
                .andExpect(jsonPath("$.body", is("print('js')")))
                .andExpect(jsonPath("$.status", is(ScriptStatus.IN_PROGRESS.toString())))
                .andExpect(jsonPath("$.output", isEmptyOrNullString()));
    }

    @Test
    public void givenScriptWithNullBody_whenStartExecution_thenReturnValidationException()
            throws Exception {

        ScriptInput scriptInput = new ScriptInput();
        scriptInput.setTimeoutSec(10_000L);

        mvc.perform(put(API_PREFIX + "/run")
                .content(asJsonString(scriptInput))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    private String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}